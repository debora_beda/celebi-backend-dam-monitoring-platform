import { v4 as uuidv4 } from 'uuid';
import { put } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function updateDams(request, response) {
  logger.info('Update dam');

  const params = {
    TableName: 'celebi-dam-monitoring',
    Item: {
      id: `${request.params.id}`,
      payload: request.body
    }
  };

  try {
    await put(params);
    response.json({ id });
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not update a dam' });
  }
}

export { updateDams };