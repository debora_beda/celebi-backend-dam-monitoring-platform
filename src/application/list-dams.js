import { scan } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function listDams(request, response) {
  logger.info('List dams');

  const params = {
    TableName: 'celebi-dam-monitoring',
  };

  try {
    const result = await scan(params);
    response.json(result);
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not list dams' });
  }
}

export { listDams };