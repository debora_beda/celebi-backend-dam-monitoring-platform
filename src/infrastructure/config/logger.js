export class Logger {

  constructor() {
    this.context = {
      applicationName: 'celebi-backend-assets-platform'
    };
  }

  configureContext(logContext) {
    this.context = logContext;
  }

  info(message) {
    let temp = {...this.context};
    temp['message'] = message;
    console.info(JSON.stringify(temp));
  }

  error(message) {
    let temp = {...this.context};
    temp['message'] = message;
    console.info(JSON.stringify(temp));
  }
}