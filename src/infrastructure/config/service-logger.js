import { Logger } from './logger';

export const serviceLogger = (function () {

  let loggerInstance;

  function createInstance() {
      const logger = new Logger();
      logger.configureContext({ application: 'celebi-backend-assets'})
      return logger;
  }

  return {
      getInstance: function () {
          if (!loggerInstance) {
            loggerInstance = createInstance();
          }
          return loggerInstance;
      },
      configureContext: function (context) {
        loggerInstance.configureContext(context);
      }
  };
})();