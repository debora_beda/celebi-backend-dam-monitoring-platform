import axios from "axios";
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

export const handler = async (event) => {
    logger.info(JSON.stringify(event.Records));

    const security = axios.create({
      baseURL: "https://dpksd3aqx2.execute-api.us-east-1.amazonaws.com/dev"
    });

    const payload = event.Records[0].dynamodb.NewImage.payload;

    const barragem = payload.M.name.S;
    const estado = payload.M.status.S;

    if(estado === 'Perigo') {
      const alert = {
        subject: `Alerta: Barragem ${barragem} em estado de ${estado}`,
        message: `Alerta enviado pelo Sistema Celebi,
        Dados coletados pelos sensores informam anormalidades na barragem ${barragem}.`
      } 

      await security.post(`/alert`, alert);
    }
};

module.exports.handler = handler;