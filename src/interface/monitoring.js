import Router from 'express';
import { serviceLogger } from '../infrastructure/config/service-logger';
import { updateDams } from '../application/update-dams';
import { listDams } from '../application/list-dams';

const damMonitoringRoutes = Router();
const logger = serviceLogger.getInstance();

damMonitoringRoutes.get("/status", async (request, response) => {
  logger.info('status get')
  await listDams(request, response);
})

damMonitoringRoutes.post("/status/:id", async (request, response) => {
  logger.info('status post')
  await updateDams(request, response);
})

export default damMonitoringRoutes;